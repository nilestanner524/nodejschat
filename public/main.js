angular.module('chat',[])
 .controller('main',['$scope','$window','$http',function(scope,$window,http){
   scope.test = "hello world";
   scope.chat = [];
   scope.username = '';
   scope.sendMessage = function(){
     console.log('message sent');
     window.location = "/chat?name=" + scope.username + "&line=" + scope.message;
   }
   function update(){
     http.get('/update').then(function(data){
       scope.chat = data.data;
     },function(error){
       console.log(error);
     });
   }
   setInterval(update,3000);
 }]);
