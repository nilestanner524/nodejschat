// Setup basic express server
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var port = process.env.PORT || 9020;

var chatBuffer = [];

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

// Routing
app.use(express.static(__dirname + '/public'));
app.get('/chat', function(req, res){
  console.log("message was sent");
  var lastMessage = {
    username: req.query.name,
    message: req.query.line
  };
  chatBuffer.push(lastMessage);
});

app.get('/update',function(req,res){
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(chatBuffer));
});
